#!/bin/bash

# -----------------------------------------------------------------------------
# Nanospace project: Nanospace Docker images installing script
# -----------------------------------------------------------------------------
# This script has to be run once in order to install the Nanospace Docker images 
# into the local Docker images repository, as they are not available through 
# DockerHub or anywhere else on the Web.
# -----------------------------------------------------------------------------

echo "Installing JSatOrb Docker images"
#DEST=../nanospace-docker-images
DEST=.


echo "-- Nanospace frontend Docker image"
gunzip -c ${DEST}/nanospace-angular-ui:dev.tgz | docker load
# Check if the command succeeded
if [ $? -ne 0 ]; then
    echo "The Docker image load failed !"
    exit 1
fi

echo "-- Nanospace neo4j Docker image"
gunzip -c ${DEST}/neo4j:3.2.tgz | docker load
# Check if the command succeeded
if [ $? -ne 0 ]; then
    echo "The Docker image load failed !"
    exit 1
fi

echo "-- Nanospace backend Docker image"
gunzip -c ${DEST}/java-service-neo4j:dev.tgz | docker load
# Check if the command succeeded
if [ $? -ne 0 ]; then
    echo "The Docker image load failed !"
    exit 1
fi

echo "Nanospace Docker images has been installed"
