# Demo with DOCKER

Welcome to the local demo of Nanospace.

## Docker build and run images on localhost

### Neo4j database

```
docker run   -it  --publish=7474:7474 --publish=7687:7687     --volume=$HOME/neo4j/data:/data  --name neo4j-container  --rm --env NEO4J_AUTH=neo4j/secret   neo4j:3.2
```

### Backend - Java service (Spingboot)

```
myIP=$(docker inspect neo4j-container | grep '"IPAddress"' | head -n 1 )
echo $myIP
IP=$(echo $myIP | sed s/\"IPAddress\":\ \"// | sed s/\",//)
echo $IP
docker run  -it  --publish=8888:8888 --volume=$HOME/neo4j/data:/data --name java-service-neo4j-container --rm --env NEO4J_URI=bolt://${IP}   java-service-neo4j:dev
```

### Frontend - Angular

```
docker run  -it  --publish=80:80 --name nanospace-angular-ui-container --rm  nanospace-angular-ui:dev 
```

And check on your favorite browser [localhost](http://localhost/)

## Deployment on a server


